<?php

namespace Core;

use PDO;
use App\Config;

/*
 * Base model
 */
abstract class Model {

	/*
	 * Get the PDO database connection
	 *
	 * @return mixed
	 */
	protected static function getDB() {
		static $db = null;

		if( $db === null ) {
			// Data Source Name
			$dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
			$db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);

			// Throw an Exception when an error occurs,
			// you can find this info in PHP documentation if you search for PDO::setAttribute
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		return $db;
	}

}